import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import axios from "axios";

Vue.use(Vuex);

const store = new Vuex.Store({
  state() {
    return {
      actionRequests: [],
    };
  },
  mutations: {
    SET_ACTION_REQUESTS(state, payload) {
      state.actionRequests = payload;
    },
  },
  actions: {
    fetchActionRequests(context) {
      return axios({
        method: "get",
        url: "http://127.0.0.1:8000/api/v1/tasks/action_requests",
        headers: {
          Authorization:
            "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoyMDk4Njk5Nzk1LCJqdGkiOiIzN2I5OWE3N2FkOTE0Njk3YjU2ODFhYTk1YmNhNmYxMCIsInVzZXJfaWQiOjE1NH0.W0L0A6bwgU0o7XMKhw3irmwJj-wHbbj5FoKrH-_Sloc",
        },
      }).then((resp) => {
        context.commit("SET_ACTION_REQUESTS", resp.data);
        console.log(resp.data);
      });
    },
  },
  getters: {},
});

const app = new Vue({
  store,
  render: (h) => h(App),
});
app.$mount("#app");
